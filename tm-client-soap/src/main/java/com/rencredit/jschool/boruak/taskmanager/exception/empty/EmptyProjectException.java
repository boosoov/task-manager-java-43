package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class EmptyProjectException extends AbstractClientException {

    public EmptyProjectException() {
        super("Error! Project not exist...");
    }

}

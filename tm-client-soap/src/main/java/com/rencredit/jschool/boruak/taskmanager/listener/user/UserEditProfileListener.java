package com.rencredit.jschool.boruak.taskmanager.listener.user;

import com.rencredit.jschool.boruak.taskmanager.endpoint.soap.*;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class UserEditProfileListener extends AbstractListener {

    @Autowired
    private AuthEndpoint authEndpoint;

    @Autowired
    private UserEndpoint userEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "edit-profile";
    }

    @NotNull
    @Override
    public String description() {
        return "Edit profile.";
    }

    @Override
    @EventListener(condition = "@userEditProfileListener.name() == #event.command")
    public void handle(final ConsoleEvent event) throws EmptyIdException_Exception, DeniedAccessException_Exception, UnknownUserException_Exception, EmptyLoginException_Exception, NotExistUserException_Exception, BusyLoginException_Exception, EmptyUserException_Exception {
        System.out.println("Enter email");
        @NotNull final String email = TerminalUtil.nextLine();
        System.out.println("Enter first name");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter last name");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter middle name");
        @NotNull final String middleName = TerminalUtil.nextLine();

        final UserDTO user = authEndpoint.profile();
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        userEndpoint.updateUser(user);

        final UserDTO userNew = authEndpoint.profile();
        System.out.println(userNew);
        System.out.println("OK");
    }

}

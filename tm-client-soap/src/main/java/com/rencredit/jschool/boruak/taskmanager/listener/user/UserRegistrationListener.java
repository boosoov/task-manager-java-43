package com.rencredit.jschool.boruak.taskmanager.listener.user;

import com.rencredit.jschool.boruak.taskmanager.endpoint.soap.*;
import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class UserRegistrationListener extends AbstractListener {

    @Autowired
    private AuthEndpoint authEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "registration";
    }

    @NotNull
    @Override
    public String description() {
        return "Registration in system.";
    }

    @Override
    @EventListener(condition = "@userRegistrationListener.name() == #event.command")
    public void handle(final ConsoleEvent event) throws BusyLoginException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception {
        System.out.println("Enter login");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("Enter password");
        @NotNull final String password = TerminalUtil.nextLine();

        authEndpoint.registrationLoginPassword(login, password);
        System.out.println("OK");
    }

}

package com.rencredit.jschool.boruak.taskmanager.integration.endpoint;

import com.rencredit.jschool.boruak.taskmanager.config.ClientConfiguration;
import com.rencredit.jschool.boruak.taskmanager.endpoint.soap.*;
import com.rencredit.jschool.boruak.taskmanager.marker.IntegrationWithServerTestCategory;
import com.rencredit.jschool.boruak.taskmanager.service.AuthService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ClientConfiguration.class)
@Category(IntegrationWithServerTestCategory.class)
public class AuthEndpointTest {

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @Autowired
    private AuthService authService;

    @Autowired
    private AuthEndpoint authEndpoint;

    @Before
    public void init() throws EmptyLoginException_Exception, DeniedAccessException_Exception, UnknownUserException_Exception, EmptyPasswordException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, EmptyRoleException_Exception, BusyLoginException_Exception {
        authService.login("admin", "admin");
        adminUserEndpoint.clearAllUser();
        authService.login("admin", "admin");
    }

    @After
    public void clearAll() throws DeniedAccessException_Exception, EmptyHashLineException_Exception, EmptyRoleException_Exception, EmptyLoginException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, UnknownUserException_Exception {
        authService.login("admin", "admin");
        adminUserEndpoint.clearAllUser();
    }

    @Test
    public void testLogin() throws DeniedAccessException_Exception, EmptyUserException_Exception, EmptyLoginException_Exception, UnknownUserException_Exception, EmptyIdException_Exception {
        assertTrue(authEndpoint.login("test", "test").isSuccess());
        assertNotNull(authEndpoint.profile());
    }

    @Test
    public void testLogout() throws DeniedAccessException_Exception, EmptyUserException_Exception, EmptyLoginException_Exception, UnknownUserException_Exception, EmptyIdException_Exception {
        assertTrue(authEndpoint.login("test", "test").isSuccess());
        assertNotNull(authEndpoint.profile());
        assertTrue(authEndpoint.logout().isSuccess());
    }

    @Test
    public void testProfile() throws DeniedAccessException_Exception, EmptyUserException_Exception, EmptyLoginException_Exception, UnknownUserException_Exception, EmptyIdException_Exception {
        assertTrue(authEndpoint.login("test", "test").isSuccess());
        assertEquals("test", authEndpoint.profile().getLogin());
    }

    @Test
    public void testRegistrationLoginPassword() throws DeniedAccessException_Exception, EmptyUserException_Exception, EmptyLoginException_Exception, UnknownUserException_Exception, EmptyIdException_Exception, EmptyHashLineException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception {
        authEndpoint.registrationLoginPassword("12345", "12345");
        assertTrue(authEndpoint.login("12345", "12345").isSuccess());
        assertEquals("12345", authEndpoint.profile().getLogin());
    }

}

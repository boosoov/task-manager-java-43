package com.rencredit.jschool.boruak.taskmanager.controller;

import com.rencredit.jschool.boruak.taskmanager.dto.CustomUser;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import com.rencredit.jschool.boruak.taskmanager.service.AuthService;
import com.rencredit.jschool.boruak.taskmanager.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {

    @Autowired
    private AuthService authService;

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping("/user/registration")
    public ModelAndView registration(
            @AuthenticationPrincipal CustomUser user
    ) {
        final UserDTO userForRegistration = new UserDTO();
        return new ModelAndView("user/user-registration", "userForRegistration", userForRegistration);
    }

    @PostMapping("/user/registration")
    public String registration(
            @ModelAttribute("userForRegistration") UserDTO userForRegistration,
            BindingResult result,
            @AuthenticationPrincipal CustomUser user
    ) throws EmptyHashLineException, BusyLoginException, EmptyPasswordException, EmptyLoginException, EmptyUserException, DeniedAccessException {
        authService.registration(userForRegistration.getLogin(), userForRegistration.getPasswordHash());
        return "redirect:/login";
    }

    @GetMapping("/user/edit")
    public ModelAndView edit(
            @AuthenticationPrincipal CustomUser user
    ) throws EmptyIdException {
        final UserDTO userForEdit = userService.findByIdDTO(user.getUserId());
        return new ModelAndView("user/user-edit", "userForEdit", userForEdit);
    }

    @PostMapping("/user/edit")
    public String edit(
            @ModelAttribute("userForEdit") UserDTO userForEdit,
            BindingResult result,
            @AuthenticationPrincipal CustomUser user
    ) throws EmptyIdException, EmptyLoginException, EmptyUserException, UnknownUserException {
        final UserDTO userFromBase = userService.findByIdDTO(user.getUserId());
        boolean isPasswordDifferent = !userFromBase.getPasswordHash().equals(userForEdit.getPasswordHash());
        if (userForEdit.getPasswordHash() != null && isPasswordDifferent) {
            final String newHash = passwordEncoder.encode(userForEdit.getPasswordHash());
            userFromBase.setPasswordHash(newHash);
        }
        if(userForEdit.getEmail() != null) userFromBase.setEmail(userForEdit.getEmail());
        if(userForEdit.getFirstName() != null) userFromBase.setFirstName(userForEdit.getFirstName());
        if(userForEdit.getLastName() != null) userFromBase.setLastName(userForEdit.getLastName());
        if(userForEdit.getMiddleName() != null) userFromBase.setMiddleName(userForEdit.getMiddleName());
        userService.rewrite(userFromBase);
        return "redirect:/";
    }

}

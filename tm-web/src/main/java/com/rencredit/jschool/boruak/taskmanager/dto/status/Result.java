package com.rencredit.jschool.boruak.taskmanager.dto.status;

import org.jetbrains.annotations.NotNull;

public class Result {

    public Result() {
    }

    public Result(@NotNull Boolean success) {
        this.success = success;
    }

    @NotNull
    public Boolean success = true;

    @NotNull
    public String message = "";

}

package com.rencredit.jschool.boruak.taskmanager.enumerated;

import org.jetbrains.annotations.NotNull;

public enum Role {

    USER("USER"),
    ADMIN("ADMIN");

    @NotNull
    private final String displayName;

    Role(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}

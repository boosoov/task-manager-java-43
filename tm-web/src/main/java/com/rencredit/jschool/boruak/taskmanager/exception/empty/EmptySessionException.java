package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractException;

public class EmptySessionException extends AbstractException {

    public EmptySessionException() {
        super("Error! Session is empty...");
    }

}

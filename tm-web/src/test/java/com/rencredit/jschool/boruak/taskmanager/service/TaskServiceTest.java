package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.config.WebApplicationConfiguration;
import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
public class TaskServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private ProjectService projectService;

    private User admin;

    private User test;

    @Before
    public void setup() throws EmptyLoginException, EmptyHashLineException, BusyLoginException, EmptyRoleException, EmptyPasswordException, EmptyUserException, DeniedAccessException {
        SecurityContext ctx = SecurityContextHolder.createEmptyContext();
        SecurityContextHolder.setContext(ctx);
        ctx.setAuthentication(new UsernamePasswordAuthenticationToken("admin", "", Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"))));

        userService.deleteAll();
        projectService.deleteAll();
        taskService.deleteAll();
        admin = userService.findByLoginEntity("admin");
        test = userService.findByLoginEntity("test");
    }

    @Test
    public void testSaveUserIdName() throws EmptyNameException, EmptyUserIdException {
        taskService.save(admin.getId(), "task");
        assertNotNull(taskService.findOneByNameDTO(admin.getId(), "task"));
    }

    @Test
    public void testSaveUserIdNameDescription() throws EmptyNameException, EmptyUserIdException, EmptyDescriptionException {
        taskService.save(admin.getId(), "task", "description");
        final TaskDTO task = taskService.findOneByNameDTO(admin.getId(), "task");
        assertNotNull(task);
        assertEquals("description", task.getDescription());
    }

    @Test
    public void testSaveTask() throws EmptyNameException, EmptyUserIdException, EmptyDescriptionException, EmptyTaskException {
        final TaskDTO task = new TaskDTO(admin.getId(),"task");
        taskService.save(task);
        assertNotNull(taskService.findOneByNameDTO(admin.getId(), "task"));
    }

    @Test
    public void testSaveList() throws EmptyUserIdException, EmptyTaskListException {
        final List<TaskDTO> list = new ArrayList<>();
        list.add(new TaskDTO(admin.getId(),"task1"));
        list.add(new TaskDTO(admin.getId(),"task2"));

        taskService.saveList(list);
        assertEquals(2, taskService.findAllByUserIdDTO(admin.getId()).size());
    }



    @Test
    public void testFindAllByUserIdDTO() throws EmptyNameException, EmptyUserIdException {
        taskService.save(admin.getId(), "task1");
        taskService.save(admin.getId(), "task2");
        taskService.save(admin.getId(), "task3");
        taskService.save(test.getId(), "task4");

        assertEquals(3, taskService.findAllByUserIdDTO(admin.getId()).size());
        assertEquals(1, taskService.findAllByUserIdDTO(test.getId()).size());
    }

    @Test
    public void testFindAllByUserIdEntity() throws EmptyNameException, EmptyUserIdException {
        taskService.save(admin.getId(), "task1");
        taskService.save(admin.getId(), "task2");
        taskService.save(admin.getId(), "task3");
        taskService.save(test.getId(), "task4");

        assertEquals(3, taskService.findAllByUserIdEntity(admin.getId()).size());
        assertEquals(1, taskService.findAllByUserIdEntity(test.getId()).size());
    }

    @Test
    public void testFindAllByProjectIdDTO() throws EmptyNameException, EmptyUserIdException, EmptyTaskException, EmptyProjectException, EmptyTaskIdException, EmptyIdException, EmptyProjectIdException {
        final TaskDTO task1 = new TaskDTO(admin.getId(),"task1");
        final TaskDTO task2 = new TaskDTO(admin.getId(),"task2");
        final ProjectDTO project = new ProjectDTO(admin.getId(),"task2");
        taskService.save(task1);
        taskService.save(task2);
        projectService.save(admin.getId(), project);
        taskService.attachTaskToProject(admin.getId(), task1.getId(), project.getId());
        taskService.attachTaskToProject(admin.getId(), task2.getId(), project.getId());

        assertEquals(2, taskService.findAllByProjectIdDTO(project.getId()).size());
    }

    @Test
    public void testFindByIndexDTO() throws EmptyNameException, EmptyUserIdException, IncorrectIndexException {
        taskService.save(admin.getId(), "task");

        assertEquals("task", taskService.findOneByIndexDTO(admin.getId(), 0).getName());
    }

    @Test
    public void testFindByNameDTO() throws EmptyNameException, EmptyUserIdException {
        taskService.save(admin.getId(), "task");

        assertEquals("task", taskService.findOneByNameDTO(admin.getId(), "task").getName());
    }

    @Test
    public void testFindByIdDTO() throws EmptyUserIdException, EmptyTaskException, EmptyIdException {
        final TaskDTO task = new TaskDTO(admin.getId(),"task");
        taskService.save(task);
        assertNotNull(taskService.findOneByIdDTO(admin.getId(), task.getId()));
    }

    @Test
    public void testFindListEntity() throws EmptyNameException, EmptyUserIdException {
        taskService.save(admin.getId(), "task1");
        taskService.save(admin.getId(), "task2");
        taskService.save(admin.getId(), "task3");
        taskService.save(test.getId(), "task4");

        assertEquals(4, taskService.findListEntity().size());
    }



    @Test
    public void testAttachTaskToProject() throws EmptyUserIdException, EmptyTaskException, EmptyProjectException, EmptyTaskIdException, EmptyIdException, EmptyProjectIdException {
        final TaskDTO task = new TaskDTO(admin.getId(),"task1");
        final ProjectDTO project = new ProjectDTO(admin.getId(),"task2");
        taskService.save(task);
        projectService.save(admin.getId(), project);
        taskService.attachTaskToProject(admin.getId(), task.getId(), project.getId());
        final TaskDTO taskFromBase = taskService.findOneByIdDTO(admin.getId(), task.getId());
        assertEquals(project.getId(), taskFromBase.getProject());
    }

    @Test
    public void testDetachTaskToProject() throws EmptyUserIdException, EmptyTaskException, EmptyProjectException, EmptyTaskIdException, EmptyIdException, EmptyProjectIdException {
        final TaskDTO task = new TaskDTO(admin.getId(),"task1");
        final ProjectDTO project = new ProjectDTO(admin.getId(),"task2");
        taskService.save(task);
        projectService.save(admin.getId(), project);
        taskService.attachTaskToProject(admin.getId(), task.getId(), project.getId());
        final TaskDTO taskFromBaseAttached = taskService.findOneByIdDTO(admin.getId(), task.getId());
        assertEquals(project.getId(), taskFromBaseAttached.getProject());

        taskService.detachTaskToProject(admin.getId(), task.getId(), project.getId());
        final TaskDTO taskFromBaseDetached = taskService.findOneByIdDTO(admin.getId(), task.getId());
        assertNull(taskFromBaseDetached.getProject());
    }

    @Test
    public void testExistsById() throws EmptyUserIdException, EmptyTaskException, EmptyIdException, EmptyTaskIdException {
        final TaskDTO task = new TaskDTO(admin.getId(),"task");
        taskService.save(task);
        assertTrue(taskService.existsById(task.getId()));
        assertFalse(taskService.existsById("34234234"));
    }

    @Test
    public void testExistsByUserIdAndTaskId() throws EmptyUserIdException, EmptyTaskException, EmptyIdException, EmptyTaskIdException {
        final TaskDTO task = new TaskDTO(admin.getId(),"task");
        taskService.save(task);
        assertTrue(taskService.existsByUserIdAndTaskId(admin.getId(), task.getId()));
        assertFalse(taskService.existsByUserIdAndTaskId(admin.getId(),"34234234"));
    }



    @Test
    public void testCount() throws EmptyNameException, EmptyUserIdException {
        taskService.save(admin.getId(), "task1");
        taskService.save(admin.getId(), "task2");
        taskService.save(admin.getId(), "task3");
        taskService.save(test.getId(), "task4");

        assertEquals(4, taskService.count());
    }

    @Test
    public void testCountAllByUserId() throws EmptyNameException, EmptyUserIdException {
        taskService.save(admin.getId(), "task1");
        taskService.save(admin.getId(), "task2");
        taskService.save(admin.getId(), "task3");
        taskService.save(test.getId(), "task4");

        assertEquals(3, taskService.countAllByUserId(admin.getId()));
        assertEquals(1, taskService.countAllByUserId(test.getId()));
    }



    @Test
    public void testDeleteUserIdTask() throws EmptyUserIdException, EmptyTaskException, EmptyIdException {
        final TaskDTO task = new TaskDTO(admin.getId(),"task");
        taskService.save(task);
        assertNotNull(taskService.findOneByIdDTO(admin.getId(), task.getId()));

        taskService.deleteById(admin.getId(), task.getId());
        assertNull(taskService.findOneByIdDTO(admin.getId(), task.getId()));
    }

    @Test
    public void testDeleteAllByUserId() throws EmptyNameException, EmptyUserIdException {
        taskService.save(admin.getId(), "task1");
        taskService.save(admin.getId(), "task2");
        taskService.save(admin.getId(), "task3");
        taskService.save(test.getId(), "task4");

        assertEquals(3, taskService.countAllByUserId(admin.getId()));
        assertEquals(1, taskService.countAllByUserId(test.getId()));
        taskService.deleteAllByUserId(admin.getId());

        assertEquals(0, taskService.countAllByUserId(admin.getId()));
        assertEquals(1, taskService.countAllByUserId(test.getId()));
    }

    @Test
    public void testDeleteOneByIndex() throws EmptyNameException, EmptyUserIdException, EmptyTaskException, IncorrectIndexException {
        taskService.save(admin.getId(), "task1");
        taskService.save(admin.getId(), "task2");
        taskService.save(admin.getId(), "task3");
        taskService.save(test.getId(), "task4");

        assertEquals(3, taskService.countAllByUserId(admin.getId()));
        assertEquals(1, taskService.countAllByUserId(test.getId()));
        taskService.deleteByIndex(admin.getId(), 0);

        assertNull(taskService.findOneByNameDTO(admin.getId(),"task1"));
        assertEquals(2, taskService.countAllByUserId(admin.getId()));
        assertEquals(1, taskService.countAllByUserId(test.getId()));
    }

    @Test
    public void testDeleteOneByName() throws EmptyNameException, EmptyUserIdException, EmptyTaskException, IncorrectIndexException {
        taskService.save(admin.getId(), "task1");
        taskService.save(admin.getId(), "task2");
        taskService.save(admin.getId(), "task3");
        taskService.save(test.getId(), "task4");

        assertEquals(3, taskService.countAllByUserId(admin.getId()));
        assertEquals(1, taskService.countAllByUserId(test.getId()));
        taskService.deleteByName(admin.getId(), "task2");

        assertNull(taskService.findOneByNameDTO(admin.getId(),"task2"));
        assertEquals(2, taskService.countAllByUserId(admin.getId()));
        assertEquals(1, taskService.countAllByUserId(test.getId()));
    }

    @Test
    public void testDeleteAll() throws EmptyNameException, EmptyUserIdException, EmptyTaskException, IncorrectIndexException, EmptyIdException, EmptyTaskIdException {
        taskService.save(admin.getId(), "task1");
        taskService.save(admin.getId(), "task2");
        taskService.save(admin.getId(), "task3");
        taskService.save(test.getId(), "task4");

        assertEquals(4, taskService.count());
        taskService.deleteAll();
        assertEquals(0, taskService.count());
    }

    @Test
    public void testDeleteList() throws EmptyTaskListException {
        final List<TaskDTO> list1 = new ArrayList<>();
        final TaskDTO task1 = new TaskDTO(admin.getId(),"task1");
        final TaskDTO task2 = new TaskDTO(admin.getId(),"task2");
        final TaskDTO task3 = new TaskDTO(admin.getId(),"task3");
        list1.add(task1);
        list1.add(task2);
        list1.add(task3);
        taskService.saveList(list1);
        assertEquals(3, taskService.count());

        final List<Task> list2 = new ArrayList<>();
        final Task taskEntity1 = new Task();
        final Task taskEntity2 = new Task();
        taskEntity1.setId(task1.getId());
        taskEntity2.setId(task2.getId());
        list2.add(taskEntity1);
        list2.add(taskEntity2);
        taskService.deleteList(list2);
        assertEquals(1, taskService.count());
    }

}
